# Training Program: Technology Stack
> Please Review Progress Matrix below

## Spring Framework

> Deliverable: Working code needs to be checked in github

[Spring, Hibernate, Web Services, Spring Boot](https://cognizant.udemy.com/course/spring-hibernate-tutorial/)

- Important topics to be covered
  - Spring Core (XML Configurations, Annotations, Dependency Injection, Bean Scope and Lifecycle)
  - Spring MVC
  - Spring AOP
  - Spring REST
  - Spring Boot

**Instructions:**

- Create a account of https://github.com/
- You can refer to any course on https://cognizant.udemy.com/
- A sample program/code needs to be checkedin in github for all the above concepts

## JMS

> Deliverable: Working code needs to be checked in github

[Messaging JMS](https://cognizant.udemy.com/course/java-message-service-jms-fundamentals/)

- Important topics to be covered
  - Setting up messaging server
  - Read/Write messages from queues and Topics
  - Understanding messaging concepts (durability, acknowledgement, filtering etc.)

**Instructions:**

- Create a account of https://github.com/
- You can refer to any course on https://cognizant.udemy.com/
- A sample program/code needs to be checkedin in github for all the above concepts


## Apache Camel

> Deliverable: Working code needs to be checked in github

[Apache Camel](https://www.udemy.com/course/apache-camel-for-beginners-learn-by-coding-in-java/)  
[Apache Camel with Spring Boot](https://www.udemy.com/course/apache-camel-learn-by-coding-in-spring-boot/)

- Important topics to be covered
  - Camel Architecture
  - Coding Camel Route
  - Transformation using Camel
  - Enterprise Integration Patterns
    - JMS to DB
    - REST to DB

**Instructions:**

- Create a account of https://github.com/
- You can refer to any course on https://cognizant.udemy.com/
- A sample program/code needs to be checkedin in github for all the above concepts

## EC2

[AWS](https://cognizant.udemy.com/course/aws-certified-cloud-practitioner-training-course)

- Important topics to be covered
  - Complete entire course, and have good understanding AWS concepts
  - Try all the hands on in lab


## JBoss Adiministration

[JBoss](https://www.udemy.com/course/jboss-eap-basics-administrationconfiguration-development)

- Important topics to be covered
  - Understand JBoss Architecture
  - Installation of JBoss
  - Understanding the directory structure
  - Understanding different modes (standalone and domain)
  - Loggin
  - Deploying war, ear file in JBoss
  - Simple Program to read/write message from JBoss Queues

## Linux Basics

[Linux](https://cognizant.udemy.com/course/linux-command-line-volume1/)

- Using VI editor
- Working with file
- Command

---

## Progress Matrix

### Anjana.K4@cognizant.com

| Course Name | Start Date | End Date | Topics Covered | Git Code | Demoable | Status |
|:------------|:-----------|:---------|:---------------|:---------|:---------|:-------|
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |

---

### Swati.Rani2@cognizant.com

| Course Name | Start Date | End Date | Topics Covered | Git Code | Demoable | Status |
|:------------|:-----------|:---------|:---------------|:---------|:---------|:-------|
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |

---
### Aparna.Singh2@cognizant.com

| Course Name | Start Date | End Date | Topics Covered | Git Code | Demoable | Status |
|:------------|:-----------|:---------|:---------------|:---------|:---------|:-------|
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |
|             |            |          |                |          |          |        |